/*
 * CameraView.hpp
 *
 *  Created on: 19 juin 2012
 *      Author: leduc
 */

#ifndef CAMERAVIEW_HPP_
#define CAMERAVIEW_HPP_

#include <QImage>
#include <QGLWidget>

#include <vector>

//#include <StdSize.hpp>
#include <AbstractModule.hpp>

class CameraView : public QGLWidget, public AbstractModule
{
public:
	CameraView(QWidget * parent = NULL);
	virtual ~CameraView();



protected:
	virtual void newMessage();

  void display(std::vector<u8> & frame);
  void paintGL();

	QImage m_img, m_glData;

};

#endif /* CAMERAVIEW_HPP_ */
