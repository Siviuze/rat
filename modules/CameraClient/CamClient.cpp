#include "CamClient.hpp"


CamClient::CamClient(asio::io_service & io_service)
  : _socket(io_service)
  , _buffer(4)
{

}


void CamClient::open(const std::string& host, u16 port)
{
  tcp::endpoint endpoint(address_v4::from_string(host), port);

  _socket.async_connect(endpoint,
      bind(&CamClient::handle_connect, this,
          asio::placeholders::error));
}

void CamClient::handle_connect(const boost::system::error_code& error)
{
  if(!error == false)
  {
    rDebug() << error.message();
    delete this;
    return;
  }

  async_read(_socket, asio::buffer(_buffer, 4),
      bind(&CamClient::handle_readHeader, this,
          asio::placeholders::error,
          asio::placeholders::bytes_transferred));
}

void CamClient::handle_readHeader(const boost::system::error_code& error, size_t bytes_transferred)
{
  if(!error == false)
  {
    rDebug() << error.message();
    delete this;
    return;
  }

  u32 size = ((_buffer[0]) << 24) |
      ((_buffer[1]) << 16) |
      ((_buffer[2]) << 8)  |
      (_buffer[3]);
  _buffer.resize(size);
  rDebug() << "The frame size is" << size;

  async_read(_socket, asio::buffer(_buffer, size),
      bind(&CamClient::handle_readData, this,
          asio::placeholders::error,
          asio::placeholders::bytes_transferred));
}

void CamClient::handle_readData(const boost::system::error_code& error, size_t bytes_transferred)
{
  if(!error == false)
  {
    rDebug() << error.message();
    delete this;
    return;
  }

  _data = _buffer; // save the last picture
  rDebug() << "Receiving a frame. Size is" << bytes_transferred;

  // get next frame
  async_read(_socket, asio::buffer(_buffer, 4),
      bind(&CamClient::handle_readHeader, this,
          asio::placeholders::error,
          asio::placeholders::bytes_transferred));
}
