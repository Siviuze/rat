#include <QApplication>
#include <QAsio.hpp>
#include <Broker.hpp>
#include "../tcplink/TcpClient.hpp"
#include "CameraView.hpp"
#include "../CameraServer/Camera.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    boost::asio::io_service io_service;
    QAsio asio(io_service, 30);

    Broker br;
    TcpClient cl(io_service);
    CameraView view;

    /*
    CameraConfig conf;
    conf.device = 0;
    conf.run = true;
    conf.quality = 90;
     */


    br.record(&cl);
    br.record(&view);

    //br.dispatch(conf.pack(), NULL);
    cl.open("192.168.0.14", 5000);
    view.show();


    return app.exec();
}

