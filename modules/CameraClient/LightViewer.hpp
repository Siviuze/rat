#ifndef _POLL_HPP_
#define _POLL_HPP_

#include <QAsio.hpp>
#include "CamClient.hpp"
#include "CameraView.hpp"

class LightViewer : public QAsio
{
public:
	LightViewer();
	virtual ~LightViewer() { }

protected:
  virtual void work();

private:
    CameraView view;
    TcpClient cl;
    boost::asio::io_service  _io_service;
};

#endif
