/*
 * CameraView.cpp
 *
 *  Created on: 19 juin 2012
 *      Author: leduc
 */

#include "CameraView.hpp"
#include "../CameraServer/Camera.hpp"
#include <opencv2/opencv.hpp>

using namespace cv;

CameraView::CameraView(QWidget * parent)
	: QGLWidget(parent)
	, AbstractModule("CameraView")
{

}

CameraView::~CameraView()
{
	// TODO Auto-generated destructor stub
}


void CameraView::newMessage()
{
  static Mat oldFrame;

  psRawMessage msg = getOldestMessage();

  CameraFrame frame;
  if(frame.load(msg))
  {

    Mat cvFrame = imdecode(frame.frame, 1);
    namedWindow( "Display window", CV_WINDOW_AUTOSIZE );


    Mat extract = cvFrame - oldFrame;
    imshow( "Display window", extract );
    oldFrame = cvFrame;
  }


}

void CameraView::display(std::vector<u8> & frame)
{
	if(frame.size() < 4) // A JPEG picture has 4 bytes for magic numbers
	{
		return;
	}

	if(frame[frame.size()-1] != 0xD9)
	{
		return;
	}

	m_img.loadFromData(frame.data(), frame.size(), "JPG");
}



void CameraView::paintGL()
{
	  // Resize the image to fit the widget
	  if(size().height() > 0)
	  {
		  m_img = m_img.scaledToHeight(size().height(), Qt::SmoothTransformation);
	  }

	  m_glData = QGLWidget::convertToGLFormat(m_img);
	  glDrawPixels(m_img.width(), m_img.height(),
	               GL_RGBA, GL_UNSIGNED_BYTE, m_glData.bits());
}


