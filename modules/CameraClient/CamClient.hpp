#ifndef _TCP_CLIENT_HPP_
#define _TCP_CLIENT_HPP_

#include <StdSize.hpp>
//#include <AbstractModule.hpp>
#include <RDebug.hpp>

#include <boost/bind.hpp>
#include <boost/asio.hpp>


using boost::asio::ip::tcp;
using namespace boost::asio::ip;
using namespace boost;


class CamClient
{
public:
    CamClient(asio::io_service & io_service);
    void open(const std::string & host, u16 port);

    std::vector<u8> & getFrame()
	  { return _data; }

private:

    void handle_connect(const boost::system::error_code& e);
    void handle_readData(const boost::system::error_code& e,   size_t bytes_transferred);
    void handle_readHeader(const boost::system::error_code& e, size_t bytes_transferred);
    
    /*
    * Members
    */
    tcp::socket       _socket;
    std::vector<u8>   _data;
    std::vector<u8>		_buffer;
};


#endif
