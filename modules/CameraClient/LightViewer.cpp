#include "LightViewer.hpp"

LightViewer::LightViewer()
	: QAsio(_io_service, 30)
  , cl(_io_service)
{
	cl.open("192.168.0.14", 5000);
	view.show();
}


void LightViewer::work()
{
	view.display(cl.getFrame());
	view.updateGL();
}
