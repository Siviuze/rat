/*
 * SSC32.cpp
 *
 *  Created on: 15 avr. 2012
 *      Author: leduc
 */

#include "SSC32.hpp"
#include <RDebug.hpp>

#include <sstream> // integer to string
#include <iostream>

#include <sys/time.h>


SSC32Msg::SSC32Msg()
  : AbstractMessage("SSC32", 0x0000) // variable length message, so no static checksum
{

}



bool SSC32Msg::load(psRawMessage msg)
{
  msg->take();

  // get the number of servo to pilot
  if((*msg >> servo_to_cmd) == false)
  {
    return false;
  }

  // extract values
  for(u8 i=0 ; i<servo_to_cmd ; ++i)
  {
    if((*msg >> channel[i] >> val[i] >> time[i] >> speed[i]) == false)
    {
      rDebug() << "Error when extracting values";
      return false;
    }
  }

  msg->release();
/*
  // test perf
  struct timeval start, end;
  u16 a, b, c, d;
  gettimeofday(&start, NULL);
  for(u32 i=0 ; i<30000 ; ++i)
  {
    msg->take();
    if((*msg >> a >> b >> c >> d) == false)
    {
      rDebug() << "Error when extracting values";
      return false;
    }
    msg->release();
  }
  gettimeofday(&end, NULL);

  long mtime, usec, sec;
  sec = end.tv_sec - start.tv_sec;
  usec = end.tv_usec - start.tv_usec;
  mtime = (sec * 1000.0 + usec/1000.0) + 0.5;
  rDebug() << "Time elapsed:" << mtime;
*/
  return true;
}

psRawMessage SSC32Msg::pack()
{
  psRawMessage msg = RawMessage::create(_topic);

  // push the number of servo to pilot
  if((*msg << servo_to_cmd) == false)
  {
    return RawMessage::create("error");
  }

  if(servo_to_cmd > 32)
  {
    return RawMessage::create("error");
  }

  // insert values
  for(u16 i=0 ; i<servo_to_cmd ; ++i)
  {
    if((*msg << channel[i] << val[i] << time[i] << speed[i]) == false)
    {
      return RawMessage::create("error");
    }
  }


  return msg;
}


SSC32::SSC32()
  : AbstractModule("SSC32")
{

}

SSC32::~SSC32()
{
  _serial.flush();
  _serial.close();
}


void SSC32::newMessage()
{
  psRawMessage rMsg = getOldestMessage();

  // get the values
  SSC32Msg aMsg;
  aMsg.load(rMsg);

  // create the query
  std::string query;
  for(u8 i=0 ; i<aMsg.servo_to_cmd ; ++i)
  {
    forgeQuery(query, aMsg.channel[i], aMsg.val[i], aMsg.time[i], aMsg.speed[i]);
  }

  rDebug() << query;

  // send command
  sendCmd(query);
}

bool SSC32::open(const std::string& device)
{
  return _serial.open(device, 115200);
}

void SSC32::init()
{
  std::string query;

  /* All channels at 0° */
  for(int i=0 ; i<32 ; i++)
  {
    forgeQuery(query, i, 1500);
  }

  sendCmd(query);
}

void SSC32::forgeQuery(std::string & query, u8 channel, u16 val, u16 time, u16 speed)
{
  /* Check parameters */
  if(channel > 31)
  {
    rDebug() << "SSC32: Bad channel" << channel;
    return;
  }

  if((val > 2500) || (val < 500))
  {
    rDebug() << "SSC32: Bad value" << val;
    return;
  }


  // convert integers to string
  std::stringstream schannel;
  std::stringstream sval;
  std::stringstream sspeed;
  std::stringstream stime;

  schannel << (u32)channel;
  sval << val;
  sspeed << speed;
  stime << time;

  // Forge query
  query += " #" + schannel.str();
  query += " P" + sval.str();
  if(time != 0)
  {
    query += " T" + stime.str();
  }
  if(speed != 0)
  {
    query += " S" + sspeed.str();
  }
}

void SSC32::sendCmd(std::string& query)
{
  // it is needed to validate the query on the SCC32
  query += " \r";

  u32 written = 0;
  _serial.write((const u8 *)query.c_str(), query.size(), &written);

  if(written != query.size())
  {
    rDebug() << "SSC32: Error when writing on serial port";
    rDebug() << "Write" << written << "on" << (u32)query.size();
  }
}
