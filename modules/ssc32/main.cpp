#include "SSC32.hpp"
#include "../tcplink/TcpSrv.hpp"

//#include <RThread.hpp>
#include <Broker.hpp>
#include <RDebug.hpp>

using namespace boost;

/*
class Logger : public AbstractModule
{
public:
  void test()
  {
    SSC32Msg msg;
    msg.servo_to_cmd = 2;

    msg.channel[0]  = 30;
    msg.val[0]      = 2200;
    msg.time[0]     = 0;
    msg.speed[0]    = 0;

    msg.channel[1]  = 31;
    msg.val[1]      = 2000;
    msg.time[1]     = 0;
    msg.speed[1]    = 0;

    sendMsg(msg.pack());
  }
};


int main()
{
  Broker b;

  SSC32 ssc;

  Logger log;
  b.record(&ssc);
  b.record(&log);

  rDebug() << ssc.open("/dev/ttyUSB0");
  ssc.init();

  log.test();
  RThread::msleep(1000);
  ssc.init();

  return 0;
}
*/



int main()
{
  Broker b;

  asio::io_service io_service;

  TcpSrv srv(io_service);
  SSC32 ssc;

  b.record(&srv);
  b.record(&ssc);

  ssc.open("/dev/ttySMX2");
  ssc.init();

  io_service.run();

  return 0;
}

