/*
 * SSC32.hpp
 *
 *  Created on: 15 avr. 2012
 *      Author: leduc
 */

#ifndef SSC32_HPP_
#define SSC32_HPP_

#include <AbstractModule.hpp>
#include <Serial.hpp>

class SSC32Msg : public AbstractMessage
{
public:
  SSC32Msg();

  virtual bool load(psRawMessage msg);
  virtual psRawMessage pack();

  // number of servo to pilot simultaneously
  u16 servo_to_cmd;

  // values ton pilot servos (one per id)
  u16 channel[32];
  u16 val[32];
  u16 time[32];
  u16 speed[32];
};


class SSC32 : public AbstractModule
{
public:
  SSC32();
  virtual ~SSC32();

  virtual void newMessage();
  bool open(const std::string & device);
  void init();

private:
  void forgeQuery(std::string & query, u8 channel, u16 val, u16 time=0, u16 speed=0);
  void sendCmd(std::string & query);

  Serial _serial;
};

#endif /* SSC32_HPP_ */
