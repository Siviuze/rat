/*
 * Joystick.cpp
 *
 *  Created on: 16 avr. 2012
 *      Author: leduc
 */

#include "Joystick.hpp"
#include <SFML/Window/Joystick.hpp>
#include <iostream>
#include <RDebug.hpp>

Joystick::Joystick()
{
  // TODO Auto-generated constructor stub

}

Joystick::~Joystick()
{
  // TODO Auto-generated destructor stub
}

void Joystick::run()
{
  while(1)
  {
    sf::Joystick::update();
    if(sf::Joystick::isConnected(0) == false)
    {
      break;
    }

    //float posX = sf::Joystick::getAxisPosition(0, sf::Joystick::X);
    //float posY = sf::Joystick::getAxisPosition(0, sf::Joystick::Y);
    //float posZ = sf::Joystick::getAxisPosition(0, sf::Joystick::Z);
    //float posR = sf::Joystick::getAxisPosition(0, sf::Joystick::R);
    float posU = sf::Joystick::getAxisPosition(0, sf::Joystick::U);
    float posV = sf::Joystick::getAxisPosition(0, sf::Joystick::V);

    //bool p0 = sf::Joystick::isButtonPressed(0, 0);
    //bool p1 = sf::Joystick::isButtonPressed(0, 1);
    //bool p2 = sf::Joystick::isButtonPressed(0, 2);
    //bool p3 = sf::Joystick::isButtonPressed(0, 3);
    //bool p4 = sf::Joystick::isButtonPressed(0, 4);
    bool p5 = sf::Joystick::isButtonPressed(0, 5);
    //bool p6 = sf::Joystick::isButtonPressed(0, 6);
    //bool p7 = sf::Joystick::isButtonPressed(0, 7);
    //bool p8 = sf::Joystick::isButtonPressed(0, 8);


    //std::cout << posX << "\t" << posY << "\t" << posZ << "\t" << posR << "\t" << posU << "\t" << posV << std::endl;
    //std::cout << p0 << "\t" << p1 << "\t" << p2 << "\t" << p3 << "\t" << p4 << "\t" << p5 << "\t" << p6 << "\t" << p7 << "\t" << p8 << std::endl;
    SSC32Msg msg;

    msg.servo_to_cmd = 2;


    msg.channel[0]  = 0;
    if(p5 == true)
    {
      msg.val[0] = 1500 + posV * 5; // +/- 500
    }
    else
    {
      msg.val[0] = 1500;
    }
    msg.time[0]     = 0;
    msg.speed[0]    = 0;

    msg.channel[1]  = 1;
    if(p5 == true)
    {
      msg.val[1] = 1500 + posU * 5; // +/- 500
    }
    else
    {
      msg.val[1] = 1500;
    }
    msg.time[1]     = 0;
    msg.speed[1]    = 0;

    //rDebug() << msg.val[0] << "\t" << msg.val[1];

    psRawMessage pMsg = msg.pack();
    /*
    psRawMessage pMsg = RawMessage::create("SSC32");
    u16 c=0x0FF0, v=0x1234, t=0x0FF0, s=0x8022;
    *pMsg << c << v << t << s;
    *pMsg << c << v << t << s;
*/
    //rDebug() << pMsg->topic();
    sendMsg(pMsg);
/*
    SSC32Msg test;
    test.load(pMsg);
    rDebug() << test.servo_to_cmd << test.val[0] << test.val[1];
*/
    rDebug() << "aie" << pMsg->getDataSize();
    for(int i=0 ;i<pMsg->getDataSize() ; i++)
    {
      printf("%x ", *(pMsg->getData() + i));
    }
    printf("\n");

    RThread::msleep(50);
  }
}
