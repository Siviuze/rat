#include <Broker.hpp>
#include "Joystick.hpp"
#include "../tcpclient/TcpClient.hpp"


int main(int argc, char *argv[])
{
  asio::io_service io_service;

  Broker b;

  TcpClient cl(io_service);
  Joystick joy;

  b.record(&cl);
  b.record(&joy);

  cl.open("10.0.0.118", 5000);
  //cl.open("127.0.0.1", 5000);
  io_service.run();
  boost::asio::io_service::work work(io_service);

  joy.launch();
  joy.join();

  return 0;
}
