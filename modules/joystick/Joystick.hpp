/*
 * Joystick.hpp
 *
 *  Created on: 16 avr. 2012
 *      Author: leduc
 */

#ifndef JOYSTICK_HPP_
#define JOYSTICK_HPP_

#include <AbstractModule.hpp>
#include <RThread.hpp>


class Joystick : public AbstractModule, public RThread
{
public:
  Joystick();
  virtual ~Joystick();

private:
  virtual void run();
};

#endif /* JOYSTICK_HPP_ */
