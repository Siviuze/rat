#include <Broker.hpp>
#include <RDebug.hpp>

#include "SpacePoint.hpp"


class Logger : public AbstractModule
{
public:
  Logger() : AbstractModule("Logger")
  { }

private:
  virtual void newMessage()
  {
    float yaw, pitch, roll;
    psRawMessage msg = getOldestMessage();

    msg->take();
    if((*msg) >> yaw >> pitch >> roll) //extract
    {
      rDebug() << yaw << "\t" << pitch << "\t" << roll; // display
    }
    msg->release();
  }
};


int main()
{
  Broker b;

  SpacePoint sp;
  sp.open("/dev/hidraw4", "/dev/hidraw5");

  Logger log;
  b.record(&sp);
  b.record(&log);

  sp.launch();
  sp.join();

  return 0;
}
