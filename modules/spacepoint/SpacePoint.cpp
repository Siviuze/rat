/*
 * SpacePoint.cpp
 *
 *  Created on: 23 mars 2012
 *      Author: leduc
 */

#include "SpacePoint.hpp"
#include <RDebug.hpp>

#include <fcntl.h>  // ::open()
#include <unistd.h> // ::close()

#include <cmath>


SpacePoint::SpacePoint()
  :AbstractModule("SpacePoint")
{
  // TODO Auto-generated constructor stub

}

SpacePoint::~SpacePoint()
{
  close();
}


void SpacePoint::open(const std::string& dfile1, const std::string& dfile2)
{
  _f1 = ::open(dfile1.c_str(), O_RDONLY);
  if(_f1 < 0)
  {
    //TODO: erreur à traiter
    rDebug() << "error while opening" << dfile1;
    perror("aie");
  }

  _f2 = ::open(dfile2.c_str(), O_RDONLY);
  if(_f2 < 0)
  {
    //TODO: erreur à traiter
    rDebug() << "error while opening " << dfile2;
    perror("aie");
  }
}


void SpacePoint::close()
{
  if(_f1 != -1)
    ::close(_f1);

  if(_f2 != -1)
    ::close(_f2);
}


void SpacePoint::writeSwap(s16 * target, const s8 * src, size_t count)
{
  for (size_t i=0; i<count; ++i)
  {
    u16 s;
    s = (u16)(src[i*2 +1]);
    s <<= 8;
    s += (u16)(src[i*2]);

    target[i] = s - 32768;
  }
}

void SpacePoint::run()
{
  while(1)
  {
    update();
    RThread::msleep(50);
  }
}


void SpacePoint::update()
{
  s8 buffer[128];
  int err;

  // read first file descriptor (raw sensors values)
  err = read(_f1, buffer, 20);
  if(err < 0)
  {
    //TODO: traiter erreur de lecture
  }

  writeSwap(_rawMagneto, buffer, 3);
  writeSwap(_rawAccelero, buffer+6, 3);
  writeSwap(_rawGyro, buffer+12, 3);

  // read second file descriptor (processed values ie. quaternion)
  err = read(_f2, buffer, 15);
  if(err < 0)
  {
    //TODO: traiter erreur de lecture
  }

  writeSwap(_scaledAccelero, buffer, 3);
  writeSwap(_quaternion, buffer+6, 4);
  _rawButtons = buffer[14];


  // extract Euler angles
  f32 q[4];
  for(int i=0 ; i<4 ; ++i)
  {
    q[i] = _quaternion[i] / 32768.0;
  }

  _roll  = atan2(2.0*(q[1]*q[2] + q[3]*q[0]),
                  2.0*q[3]*q[3] + 2.0*q[2]*q[2] - 1.0);

  _pitch = asin(2.0*(q[0]*q[2] - q[3]*q[1]));

  _yaw   = atan2(2.0*(q[0]*q[1] + q[3]*q[2]),
                  2.0*q[3]*q[3] + 2.0*q[0]*q[0] - 1.0);

  // format the values (according to sensor doc)
  _accelX = _scaledAccelero[0] * 6.0 / 32768.0;
  _accelY = _scaledAccelero[1] * 6.0 / 32768.0;
  _accelZ = _scaledAccelero[2] * 6.0 / 32768.0;

  _gyroX = _rawGyro[0] / 32768.0;
  _gyroY = _rawGyro[1] / 32768.0;
  _gyroZ = _rawGyro[2] / 32768.0;

  _magnetoX = _rawMagneto[0] / 32768.0;
  _magnetoY = _rawMagneto[1] / 32768.0;
  _magnetoZ = _rawMagneto[2] / 32768.0;

  _button1 = _rawButtons & 0x1;
  _button2 = _rawButtons & 0x2;

  SpacePointMsg msg;
  msg._yaw = _yaw;
  msg._pitch = _pitch;
  msg._roll = _roll;
  sendMsg(msg.pack());
}


SpacePointMsg::SpacePointMsg()
  :AbstractMessage("IMU", 0x361b)
{

}

bool SpacePointMsg::load(psRawMessage msg)
{
  //Nothing to do
  return false;
}

psRawMessage SpacePointMsg::pack()
{
  psRawMessage raw = RawMessage::create(_topic);
  *raw << _yaw << _pitch << _roll;
  return raw;
}


