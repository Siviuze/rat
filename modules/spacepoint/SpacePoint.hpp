/*
 * SpacePoint.hpp
 *
 *  Created on: 23 mars 2012
 *      Author: leduc
 */

#ifndef SPACEPOINT_HPP_
#define SPACEPOINT_HPP_

#include <AbstractModule.hpp>
#include <RThread.hpp>

class SpacePointMsg : public AbstractMessage
{
public:
  SpacePointMsg();

  virtual bool load(psRawMessage msg);
  virtual psRawMessage pack();

  float _yaw, _pitch, _roll;
};


class SpacePoint : public AbstractModule, public RThread
{
public:
  SpacePoint();
  virtual ~SpacePoint();

  void open(const std::string & dfile1,
            const std::string & dfile2);
  void close();

  void printData();

private:
  void writeSwap(s16 * target, const s8 * src, size_t count);
  void run();     /// Thread loop (run update periodically)
  void update();  /// Get new values of the sensor

  /* Sensor file descriptors */
  int _f1;
  int _f2;

  /* Sensor values */

  // Raw
  short _rawMagneto[3];
  short _rawAccelero[3];
  short _rawGyro[3];
  char  _rawButtons;

  // processed by the sensor
  short _scaledAccelero[3];
  short _quaternion[4];

  // extract from quaternion
  float _yaw,      _pitch,    _roll;
  float _accelX,   _accelY,   _accelZ;
  float _gyroX,    _gyroY,    _gyroZ;
  float _magnetoX, _magnetoY, _magnetoZ;
  bool  _button1,  _button2;


};

#endif /* SPACEPOINT_HPP_ */
