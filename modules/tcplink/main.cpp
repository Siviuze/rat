#include <Broker.hpp>
#include <RDebug.hpp>
#include <RThread.hpp>

#include "TcpClient.hpp"
#include "TcpSrv.hpp"

#include <time.h>

using namespace boost;

class Logger : public AbstractModule, public RThread
{
public:
  Logger() : AbstractModule("Logger")
  { }

  void run()
  {
    psRawMessage toSend = RawMessage::create("test transmission");
    time_t rawtime;
    while(1)
    {
      toSend->clear();
      time ( &rawtime );
      *toSend << "The current local time is " << ctime(&rawtime);
      sendMsg(toSend);

      RThread::msleep(1000);
    }
  }
  
  void newMessage()
  {
    psRawMessage msg = getOldestMessage();
    rDebug() << "Logger:" << msg->topic();
  }
};



int main(int argc, char *argv[])
{
  asio::io_service io_service;

  Broker b1;
  Broker b2;

  TcpClient cl(io_service);
  TcpSrv srv(io_service);
  Logger log;
  Logger logRec;

  b2.record(&cl);
  b2.record(&log);
  
  b2.record(&srv);
  b2.record(&logRec);

  cl.open("127.0.0.1", 5000);
  logRec.launch();
  
  boost::asio::io_service::work work(io_service);
  io_service.run();
  log.join();


  return 0;
}
