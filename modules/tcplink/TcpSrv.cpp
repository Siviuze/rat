
#include "TcpSrv.hpp"
#include <StdSize.hpp>

using namespace boost;

TcpSrv::TcpSrv(asio::io_service & io_service, u16 port)
  : AbstractModule("TcpSrv")
  , _io_service (io_service)
  , _acceptor   (io_service, tcp::endpoint(tcp::v4(), port))
{
  rDebug() << "Listening on port" << port;
  listen();
}


TcpSrv::~TcpSrv()
{
    for(auto it=_sessionsList.begin() ; it != _sessionsList.end() ; it++)
    {
        removeSession(*it);
        delete *it;
    }
}

void TcpSrv::listen()
{
  Session* newSession = new Session(_io_service, *this);
  _acceptor.async_accept(newSession->socket(),
                         bind(&TcpSrv::handle_listen, this, newSession,
                              asio::placeholders::error));
}

void TcpSrv::handle_listen(Session * newSession,
                              const system::error_code& error)
{
  if (!error)
  {
    _sessionsList.insert(newSession);
    rDebug() << "New session:" << newSession->socket().remote_endpoint().address().to_string(); // display origin

    newSession->startReading();
  }
  else
  {
    rDebug() << "Error:" << error.message();
    delete newSession;
  }

  // wait for next future connection
  listen();
}


void TcpSrv::removeSession(Session * session)
{
  _sessionsList.erase(session);
}


void TcpSrv::newMessage()
{
  psRawMessage msg = getOldestMessage();
  rDebug() << "TcpSrv::newMessage" << msg->topic();
  
  if(msg->topic() == "None")
  {
    return;
  }

  for(auto it=_sessionsList.begin() ; it != _sessionsList.end() ; it++)
  {
    (*it)->write(msg);
  }
}


/**************************************************************************/



Session::Session(asio::io_service& io_service, TcpSrv & server)
  : TcpCom(io_service)
  , _server(server)
{

}

Session::~Session()
{
  // clean set of sessions
  _server.removeSession(this);
}

void Session::recMsgComplete(psRawMessage rec)
{
    _server.sendMsg(rec);
}

