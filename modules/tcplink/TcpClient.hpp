#ifndef _TCP_CLIENT_HPP_
#define _TCP_CLIENT_HPP_

#include <StdSize.hpp>
#include <AbstractModule.hpp>
#include <RDebug.hpp>

#include "TcpCom.hpp"

using boost::asio::ip::tcp;



class TcpClient : public AbstractModule, public TcpCom
{
public:
  TcpClient(boost::asio::io_service & io_service);

  void open(const std::string & host, u16 port);


protected:
  virtual void newMessage();
  virtual void recMsgComplete(psRawMessage rec);

private:
  void handle_connect(const boost::system::error_code& e);

};


#endif
