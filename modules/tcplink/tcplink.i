%module pyTcplink

%import <pyengine.i>
%include <helpers.i>

%{
#include "TcpClient.hpp"
#include "TcpSrv.hpp"
%}


%include "TcpClient.hpp"
%include "TcpSrv.hpp"
