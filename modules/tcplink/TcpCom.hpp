#ifndef _TCP_COM_
#define _TCP_COM_

#include <RawMessage.hpp>
#include <RDebug.hpp>

#include <boost/bind.hpp>
#include <boost/asio.hpp>


using boost::asio::ip::tcp;

class TcpCom
{
public:
    TcpCom(boost::asio::io_service & io_service);
    
    void write(psRawMessage msg);
    void startReading();
    
    tcp::socket & socket() { return _socket; }

protected:
    virtual void recMsgComplete(psRawMessage rec) = 0;
    
    tcp::socket       _socket;
    
private:
    void readHeader();
    void handle_readHeader(const boost::system::error_code& error,
                           size_t bytes_transferred);

    void handle_readMsg(const boost::system::error_code& error,
                        size_t bytes_transferred);
                        
    void handle_writeMsg(const boost::system::error_code& error);
    
    std::vector<u8>   _data;
};



#endif