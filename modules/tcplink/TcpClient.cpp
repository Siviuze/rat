#include "TcpClient.hpp"

using namespace boost::asio::ip;
using namespace boost;

TcpClient::TcpClient(asio::io_service & io_service)
  : AbstractModule("TcpClient")
  , TcpCom(io_service)
{

}


void TcpClient::open(const std::string& host, u16 port)
{
  tcp::endpoint endpoint(address_v4::from_string(host), port);

  _socket.async_connect(endpoint,
                        bind(&TcpClient::handle_connect, this,
                             asio::placeholders::error));
}


void TcpClient::newMessage()
{
  psRawMessage msg = getOldestMessage();
  rDebug() << "TcpClient::newMessage" << msg->topic();

  msg->take();
  write(msg);
  msg->release();
}

void TcpClient::recMsgComplete(psRawMessage rec)
{
    sendMsg(rec);
}

void TcpClient::handle_connect(const boost::system::error_code& error)
{
  if(!error)
  {
    rDebug() << "Connected";
  }
  else
  {
    rDebug() << "Error:" << error.message();
  }
  
  startReading();
}