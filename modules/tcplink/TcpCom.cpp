#include "TcpCom.hpp"

using namespace boost;

TcpCom::TcpCom(boost::asio::io_service & io_service)
    : _socket(io_service)
    , _data(1024)
{

}

void TcpCom::write(psRawMessage toSend)
{
  /************** Net header *******************/
  // taille a envoyer

  static std::vector<u8> toWrite;
  toWrite.clear();

  toWrite = toSend->serialize();
  toWrite.push_back('\r');
  toWrite.push_back('\n');
  u32 size = toWrite.size();

  // header
  std::vector<u8> header(4);
  header[0] = (u8)((size & 0xFF000000) >> 24);
  header[1] = (u8)((size & 0x00FF0000) >> 16);
  header[2] = (u8)((size & 0x0000FF00) >> 8);
  header[3] = (u8)((size & 0x000000FF) >> 0);

  // write buffer sync (since its only 4 bytes)
  // TODO: fusion des deux vecteurs (= un seul async write), file d'attente message � �crire
  asio::write(_socket, asio::buffer(header, 4));

  asio::write(_socket, asio::buffer(toWrite, size));

  /*
  asio::async_write(_socket, asio::buffer(toWrite, size),
                    bind(&TcpCom::handle_writeMsg, this,
                         asio::placeholders::error));
                         */
}

void TcpCom::startReading()
{
    readHeader();
}


void TcpCom::readHeader()
{
  async_read(_socket, asio::buffer(_data, 4),
            bind(&TcpCom::handle_readHeader, this,
                asio::placeholders::error,
                asio::placeholders::bytes_transferred));
}
            

void TcpCom::handle_readHeader(const boost::system::error_code& error,
                               size_t bytes_transferred)
{
  if(!error == false)
  {
    rDebug() << "Header" << error.message();
    return;
  }

  u32 size = ((_data[0]) << 24) |
             ((_data[1]) << 16) |
             ((_data[2]) << 8)  |
             (_data[3]);


  // minimal size of 4 bytes (since its the header size of the message topic)
  if(size >= 4)
  {
    // prepare data copy
    if(_data.size() < size)
    {
      _data.resize(size);
    }

    // get msg header
    async_read(_socket, asio::buffer(_data, size),
              bind(&TcpCom::handle_readMsg, this,
                  asio::placeholders::error,
                  asio::placeholders::bytes_transferred));
  }
  else
  {
    rDebug() << "Bad header - size = " << size;
    readHeader();
  }
}

void TcpCom::handle_readMsg(const boost::system::error_code& error,
                            size_t bytes_transferred)
{
  if(!error == false)
  {
    rDebug() << "Message header" << error.message();
    delete this;
    return;
  }

  if((_data[bytes_transferred-2] != '\r') ||
     (_data[bytes_transferred-1] != '\n'))
  {
    rDebug() << "Bad delimiter" << (u32)_data[bytes_transferred-2] << (u32)_data[bytes_transferred-1];
    readHeader();
    return;
  }

  // remove the delimiter
  _data.pop_back();
  _data.pop_back();

  psRawMessage rec = RawMessage::create("");

  if(rec->unserialize(_data))
  {
    this->recMsgComplete(rec);
  }
  else
  {
    rDebug() << "Can't unserialize the message";
  }

  // get next message header
  readHeader();
}
                    

void TcpCom::handle_writeMsg(const boost::system::error_code& error)
{
  if(!error == false)
  {
    rDebug() << "Error:" << error.message();
  }
}
