#include "Camera.hpp"
#include "../tcplink/TcpSrv.hpp"
#include <Broker.hpp>

int main()
{
    boost::asio::io_service  io_service;

    Camera cam;
    TcpSrv srv(io_service);
    Broker br;

    br.record(&cam);
    br.record(&srv);

    CameraConfig conf;
    conf.device = 0;
    conf.run = true;
    conf.quality = 70;

    br.dispatch(conf.pack(), NULL);
    io_service.run();
    cam.join();

    return 0;
}


