#include "Camera.hpp"
#include <opencv2/opencv.hpp>

using namespace cv;



CameraConfig::CameraConfig()
    : AbstractMessage("CameraConfig", 0x1f0d)
{}

psRawMessage CameraConfig::pack()
{
    psRawMessage toSend = RawMessage::create(_topic);
    (*toSend) << device << quality << run;
    return toSend;
}

bool CameraConfig::load(psRawMessage toLoad)
{
    // check the topic
    if(toLoad->topic() != _topic)
    {
        rDebug() << "Bad topic";
        return false;
    }
    
    // check the fletcher checksum (order of data type in the message)
    if(toLoad->getFLetcherChecksum() != this->_fletcherChecksum)
    {
        rDebug() << "Bad checksum";
        rDebug() << "Get      " << toLoad->getFLetcherChecksum();
        rDebug() << "Should be" << this->_fletcherChecksum;
        return false;
    }
    
    
    toLoad->take();                         //lock the message acces and init the reader
    (*toLoad) >> device >> quality >> run;  // extract data
    toLoad->release();                      //unlock the message
    
    return true;
}


CameraFrame::CameraFrame()
    : AbstractMessage("CameraFrame", 0x0000)
{}

psRawMessage CameraFrame::pack()
{
    psRawMessage toSend = RawMessage::create(_topic);
    toSend->getDataVector() = frame;
    return toSend;
}

bool CameraFrame::load(psRawMessage toLoad)
{
    // check the topic
    if(toLoad->topic() != _topic)
    {
        rDebug() << "Bad topic";
        return false;
    }
    
    toLoad->take();
    frame = toLoad->getDataVector();
    toLoad->release();
    
    return true;
}



Camera::Camera()
  : AbstractModule("Camera")
  , _device     (0) // default device
  , _run        (false)
{
  _jpegConfig.push_back(CV_IMWRITE_JPEG_QUALITY);
  _jpegConfig.push_back(70); // default comrpession set to 70
}


Camera::~Camera()
{

}

void Camera::newMessage()
{
    psRawMessage incoming = getOldestMessage();
    
    CameraConfig conf;
    if(conf.load(incoming) == true)
    {
        // check if it is needed to restart the thread
        bool restart = false;
        if((_device != conf.device) ||
           (_run == false && conf.run == true))
        {
          restart = true;
        }
    
        // load data
        _device = conf.device;
        _jpegConfig[1] = conf.quality;
        _run = conf.run;
        
        // rstart thread if needed
        if(restart == true)
        {    
            this->launch();
        }
    }
}


void Camera::run()
{
    u32 error = 0;

    VideoCapture cap(_device);  // open camera
    if(!cap.isOpened())         // check if we succeeded
    {
        rCritical() << "Can't open the camera" << _device << " Aborting...";
        return;
    }
    
    while(_run)
    {
      if(error > 250)
      {
        _run = false;
        rCritical() << "Too many errors. Aborting...";
        break;
      }
    
      Mat frame;
      cap >> frame;
      if(frame.empty() == true)
      {
        rWarning() << "Error while capturing a new frame";
        error++;
        continue;
      }

      psRawMessage jpegFrame = RawMessage::create("CameraFrame");
      imencode(".jpg", frame,  jpegFrame->getDataVector(), _jpegConfig);    // use the low level access of RawMessage -> need performance
      if(jpegFrame->getDataVector().at(jpegFrame->getDataSize()-1) != 0xD9) // JPEG magic number (cf. doc)
      {
        rWarning() << "Error while encoding the frame";
        error++;
        continue;
      }
      error = 0; // reset if one frame is OK
      
      sendMsg(jpegFrame); // send the frame
      
      RThread::msleep(2);
    }
}
