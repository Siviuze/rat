/***********************************************************************
 * Antenatus - Copyright (C) 2011 Leduc Philippe                        *
 *                                                                      *
 * ledphilippe@gmail.com                                                *
 *                                                                      *
 * This file is part of Antenatus.                                      *
 *                                                                      *
 * Antenatus is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 *                                                                      *
 ***********************************************************************/



#ifndef _CAMERA_HPP_
#define _CAMERA_HPP_

#include <StdSize.hpp>
#include <AbstractModule.hpp>
#include <RThread.hpp>
#include <RDebug.hpp>

#include <set>
#include <vector>



class CameraConfig : public AbstractMessage
{
public:
    CameraConfig();

    virtual psRawMessage pack();
    virtual bool load(psRawMessage msg);

    s32     device;     // device to stream
    s32     quality;    // JPEG compression factor (100 = best)
    bool    run;        // start/stop the video stream
};


class CameraFrame : public AbstractMessage
{
public:
    CameraFrame();

    virtual psRawMessage pack();
    virtual bool load(psRawMessage msg);

    std::vector<u8> frame;
};




/**
 * \file    TcpServer.hpp
 * \author  Philippe Leduc
 * \version 1.0
 *
 * \class TcpServer
 *
 * \brief video streaming module for Antenatus
 */
class Camera : public AbstractModule, public RThread
{
public:
  Camera();
  ~Camera();

private:
  virtual void run();
  virtual void newMessage();

  std::vector<s32>        _jpegConfig;  // OpenCV JPEG comrpession structure
  u16                     _device;      // OpenCV device id
  bool                    _run;         // flag to stop the thread
};



#endif
