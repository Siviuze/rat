#ifndef _PY_HELPERS_
#define _PY_HELPERS_

#include <boost/asio.hpp>

// wrapper for python
class py_io_service : public boost::asio::io_service
{
public:
  void prun()   { this->run();  }
  void ppoll()  { this->poll(); }
};

#endif
