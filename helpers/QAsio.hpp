/*
 * QAsio.hpp
 *
 *  Created on: 20 juin 2012
 *      Author: philippe.leduc
 */

#ifndef QASIO_HPP_
#define QASIO_HPP_

#include <StdSize.hpp>
#include <QTimer>
#include <boost/asio.hpp>

/**
 * \class QAsio
 * \brief Helper class to integrate boost::asio and the Qt events loop in the same thread
 *
 */
class QAsio : public QObject
{
  Q_OBJECT

public:
  QAsio(boost::asio::io_service & io_service, u16 delay);
  virtual ~QAsio();

protected slots:
    void tickEvent();

protected:
    virtual void work();

private:
  boost::asio::io_service&  _io_service;
  QTimer                    _timer;
};

#endif /* QASIO_HPP_ */
