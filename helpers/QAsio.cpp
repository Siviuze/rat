#include "QAsio.hpp"
#include <RDebug.hpp>

QAsio::QAsio(boost::asio::io_service & io_service, u16 delay)
  : _io_service(io_service)
{
  QObject::connect(&_timer, SIGNAL(timeout()),
                   this,   SLOT(tickEvent()));
  _timer.start(delay);
}

QAsio::~QAsio()
{

}


void QAsio::tickEvent()
{
  _io_service.poll(); // Pops boost::asio events
  work();             // work is used to add specific work on the tick event
}


void QAsio::work()
{

}
