/*
 * RSet.hpp
 *
 *  Created on: 11 déc. 2011
 *      Author: leduc
 */

#ifndef RSET_HPP_
#define RSET_HPP_

#include <set>

namespace RSet
{

template<typename T>
bool contains(std::set<T> & set, const T & t)
{
  auto it = set.find(t);
  if(it != set.end())
    return true;

  return false;
}

template<typename T>
int remove(std::set<T> & set, const T & t)
{
  int i=0;

  auto it = set.find(t);
  while(it != set.end())
  {
    set.erase(it);
    i++;

    // delete the next one (if any)
    it = set.find(t);

  }

  // number of deletions
  return i;
}

}

#endif /* RSET_HPP_ */
