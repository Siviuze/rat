#include "RawMessage.hpp"
#include "Broker.hpp"

#include <cstring>
#include <netinet/in.h>

#include "RDebug.hpp"


psRawMessage RawMessage::create(const std::string & topic)
    {
      return psRawMessage(new RawMessage(topic));
    }

RawMessage::RawMessage(const std::string& type)
  : _topic      (type)
  , _isValid    (true)
  , _readPos    (0)
  , _fletcherSum1(0)
  , _fletcherSum2(0)
{

}

RawMessage::~RawMessage()
{
  rDebug() << "end of RawMessage" << this;
}


const EmitterID& RawMessage::emitter() const
{
  return _emitter;
}


const std::string& RawMessage::topic() const
{
  return _topic;
}


void RawMessage::setEmitter(const AbstractModule* pModule)
{

}

void RawMessage::setTopic(const std::string & topic)
{
  _topic = topic;
}

// before processing, module must take the message token
void RawMessage::take()
{
  _mutex.lock();  // thread safe access
  _readPos = 0;     // init the reading position
}

// after processing, module must release the token
void RawMessage::release()
{
  _mutex.unlock();
}

void RawMessage::append(const void* data, std::size_t numBytes, std::vector<u8> & toStore)
{
  if ((data != NULL) && (numBytes > 0))
  {
    std::size_t start = toStore.size();
    toStore.resize(start + numBytes);
    std::memcpy(&toStore[start], data, numBytes);
  }
}


void RawMessage::clear()
{
  _fletcherSum1     = 0;
  _fletcherSum2     = 0;

  _data.clear();
  _readPos = 0;
  _isValid = true;
}


u16 RawMessage::getFLetcherChecksum() const
{
  return ((_fletcherSum2 << 8) | _fletcherSum1);
}


std::vector<u8> & RawMessage::getDataVector() 
{
    return _data;
}

const u8* RawMessage::getData() const
{
  return _data.data();
}

std::size_t RawMessage::getDataSize() const
{
  return _data.size();
}


void RawMessage::updateFletcherChecksum(u8 fletcherID)
{
  _fletcherSum1 = (_fletcherSum1 + fletcherID)    % 255;
  _fletcherSum2 = (_fletcherSum2 + _fletcherSum1) % 255;
}


RawMessage& RawMessage::operator >>(bool & data)
{
  u8 value;
  if (*this >> value)
  {
    data = (value != 0);
  }

  return *this;
}

RawMessage& RawMessage::operator >>(s8 & data)
{
  if (checkSize(sizeof(s8)))
  {
    data = *reinterpret_cast<const s8*>(getData() + _readPos);
    _readPos += sizeof(s8);
  }

  return *this;
}

RawMessage& RawMessage::operator >>(u8 & data)
{
  if (checkSize(sizeof(u8)))
  {
    data = *reinterpret_cast<const u8*>(getData() + _readPos);
    _readPos += sizeof(u8);
  }

  return *this;
}

RawMessage& RawMessage::operator >>(s16 & data)
{
  if (checkSize(sizeof(s16)))
  {
    data = ntohs(*reinterpret_cast<const s16*>(getData() + _readPos));
    _readPos += sizeof(s16);
  }

  return *this;
}

RawMessage& RawMessage::operator >>(u16 & data)
{
  if (checkSize(sizeof(u16)))
  {
    //data = ntohs((u16)(_data[_readPos]) + (u16)(_data[_readPos+1] << 8));
    data = ntohs(*reinterpret_cast<const u16*>(_data.data() + _readPos));
    _readPos += sizeof(u16);
  }

  return *this;
}

RawMessage& RawMessage::operator >>(s32 & data)
{
  if (checkSize(sizeof(s32)))
  {
    data = ntohl(*reinterpret_cast<const s32*>(getData() + _readPos));
    _readPos += sizeof(s32);
  }

  return *this;
}

RawMessage& RawMessage::operator >>(u32 & data)
{
  if (checkSize(sizeof(u32)))
  {
    data = ntohl(*reinterpret_cast<const u32*>(getData() + _readPos));
    _readPos += sizeof(u32);
  }

  return *this;
}

RawMessage& RawMessage::operator >>(f32 & data)
{
  if (checkSize(sizeof(f32)))
  {
      data = *reinterpret_cast<const f32*>(getData() + _readPos);
      _readPos += sizeof(f32);
  }

  return *this;
}

RawMessage& RawMessage::operator >>(f64 & data)
{
  if (checkSize(sizeof(f64)))
  {
      data = *reinterpret_cast<const f64*>(getData() + _readPos);
      _readPos += sizeof(f64);
  }

  return *this;
}

RawMessage& RawMessage::operator >>(char *data)
{
  // First extract string length
  u32 length = 0;
  *this >> length;

  if ((length > 0) && checkSize(length))
  {
    // Then extract characters
    std::memcpy(data, getData() + _readPos, length);
    data[length] = '\0';

    // Update reading position
    _readPos += length;
  }

  return *this;
}

RawMessage& RawMessage::operator >>(std::string & data)
{
  // First extract string length
  u32 length = 0;
  *this >> length;

  data.clear();
  if ((length > 0) && checkSize(length))
  {
    // Then extract characters
    data.assign((const char *)getData() + _readPos, length);

    // Update reading position
    _readPos += length;
  }

  return *this;
}

RawMessage& RawMessage::operator >>(wchar_t *data)
{
  // First extract string length
  u32 length = 0;
  *this >> length;

  if ((length > 0) && checkSize(length * sizeof(wchar_t)))
  {
    // Then extract characters
    for(u32 i = 0; i < length; ++i)
    {
      u32 character = 0;
      *this >> character;
      data[i] = static_cast<wchar_t>(character);
    }
    data[length] = L'\0';
  }

  return *this;
}

RawMessage& RawMessage::operator >>(std::wstring & data)
{
  // First extract string length
  u32 length = 0;
  *this >> length;

  data.clear();
  if ((length > 0) && checkSize(length * sizeof(wchar_t)))
  {
    // Then extract characters
    for (u32 i = 0; i < length; ++i)
    {
      u32 character = 0;
      *this >> character;
      data += static_cast<wchar_t>(character);
    }
  }

  return *this;
}




RawMessage& RawMessage::operator <<(bool& data)
{
  u8 temp = static_cast<u8>(data);
  *this << temp;
  return *this;
}

RawMessage & RawMessage::operator <<(s8 & data)
{
  updateFletcherChecksum(2);

  append(&data, sizeof(s8), _data);
  return *this;
}



RawMessage & RawMessage::operator <<(u8 & data)
{
  updateFletcherChecksum(1);

  append(&data, sizeof(u8), _data);
  return *this;
}



RawMessage & RawMessage::operator <<(s16 & data)
{
  updateFletcherChecksum(4);

  s16 toWrite = htons(data);
  append(&toWrite, sizeof(s16), _data);
  return *this;
}



RawMessage & RawMessage::operator <<(u16 & data)
{
  updateFletcherChecksum(3);

  u16 toWrite = htons(data);
  append(&toWrite, sizeof(u16), _data);
  return *this;
}



RawMessage & RawMessage::operator <<(s32 & data)
{
  updateFletcherChecksum(6);

  s32 toWrite = htonl(data);
  append(&toWrite, sizeof(s32), _data);
  return *this;
}



RawMessage & RawMessage::operator <<(u32 & data)
{
  updateFletcherChecksum(5);

  u32 toWrite = htonl(data);
  append(&toWrite, sizeof(u32), _data);
  return *this;
}



RawMessage & RawMessage::operator <<(f32 & data)
{
  updateFletcherChecksum(9);

  append(&data, sizeof(f32), _data);
  return *this;
}



RawMessage & RawMessage::operator <<(f64 & data)
{
  updateFletcherChecksum(10);

  append(&data, sizeof(f64), _data);
  return *this;
}



RawMessage & RawMessage::operator <<(const char *data)
{
  updateFletcherChecksum(11);

  // First insert string length
  u32 length = 0;
  for (const char* c = data ; *c != '\0' ; ++c)
  {
    ++length;
  }
  *this << length;

  // Then insert characters
  append(data, length * sizeof(char), _data);

  return *this;
}



RawMessage & RawMessage::operator <<(const std::string & data)
{
  updateFletcherChecksum(11);

  // First insert string length
  u32 length = static_cast<u32>(data.size());

  *this << length;

  // Then insert characters
  if (length > 0)
  {
      append(data.c_str(), length * sizeof(std::string::value_type), _data);
  }

  return *this;
}



RawMessage & RawMessage::operator <<(const wchar_t *data)
{
  updateFletcherChecksum(12);

  // First insert string length
  u32 length = 0;
  for (const wchar_t* c = data; *c != L'\0'; ++c)
  {
    ++length;
  }
  *this << length;

  // Then insert characters
  for (const wchar_t* c = data; *c != L'\0'; ++c)
  {
    u32 temp = static_cast<u32>(*c);
    *this << temp;
  }

  return *this;
}



RawMessage & RawMessage::operator <<(const std::wstring & data)
{
  updateFletcherChecksum(12);

  // First insert string length
  u32 length = static_cast<u32>(data.size());
  *this << length;

  // Then insert characters
  if (length > 0)
  {
     for (std::wstring::const_iterator c = data.begin(); c != data.end(); ++c)
     {
       u32 temp = static_cast<u32>(*c);
       *this << temp;
     }
  }

  return *this;
}



bool RawMessage::checkSize(std::size_t size)
{
  _isValid = _isValid && (_readPos + size <= _data.size());
  return _isValid;
}


std::vector<u8> RawMessage::serialize()
{
  std::vector<u8> msg;
  msg.reserve(_data.size() + sizeof(u32) + _topic.size());  // taille du message avec son header

  /* Header */
  u32 topicSize = static_cast<u32>(_topic.size());
  u32 toWrite = htonl(topicSize);
  append(&toWrite, sizeof(u32), msg);
  append(_topic.c_str(), topicSize * sizeof(std::string::value_type), msg);

  /* Data */
  append(_data.data(), _data.size(), msg);

  return msg;
}



bool RawMessage::unserialize(const std::vector<u8> & msg)
{
  // clean up
  this->clear();
  u32 readPos = 0;


  /* Header extraction */
  if(msg.size() < sizeof(u32))
  {
    return false;
  }
  u32 topicSize = ntohl(*reinterpret_cast<const u32*>(msg.data() + readPos));;
  readPos += sizeof(u32);


  if((msg.size() - readPos) < topicSize)
  {
    return false;
  }
  _topic.clear();
  _topic.assign((const char*)msg.data() + readPos, topicSize);
  readPos += topicSize;

  /* Data extraction */
  append(msg.data() + readPos, msg.size() - readPos, _data);

  return true;
}
