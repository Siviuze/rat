#ifndef _BROKER_HPP_
#define _BROKER_HPP_

#include "RSet.hpp"
#include "RMap.hpp"

#include "AbstractModule.hpp"
#include "RawMessage.hpp"
#include "EmitterID.hpp"
#include "Filter.hpp"


class Broker
{

public:
  Broker(const std::string name = "broker");
  ~Broker();

  bool record(AbstractModule* pModule, s16 requestSlot=-1);
  bool remove(AbstractModule* pModule);
  
  // message filter control (by pair type/emitter)
  bool addFilter(const AbstractModule* pModule, const std::string& pMsgType, const EmitterID& emID);
  bool rmFilter (const AbstractModule* pModule, const std::string& pMsgType, const EmitterID& emID);
  
  // remove all filters
  bool rmFilters(const AbstractModule* pModule);
  
  // dispatch message to modules
  void dispatch(psRawMessage msg, AbstractModule* emitter);
  
  const std::string & name() const
  { return _name; }

private:
  void addModule(AbstractModule* pModule, s16 slot);
  
  
  std::string       _name;
  std::set<s16>     _occupiedSlots;
  
  std::map<AbstractModule*, Filter*>    _modules;
  
};


#endif // _ABSTRACT_MODULE_HPP_
