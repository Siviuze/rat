#ifndef _STD_SIZE_H_
#define _STD_SIZE_H_

#include <cstdint>

typedef uint8_t     u8;   // fletcher id : 1
typedef int8_t      s8;   // fletcher id : 2

typedef uint16_t    u16;  // fletcher id : 3
typedef int16_t     s16;  // fletcher id : 4

typedef uint32_t    u32;  // fletcher id : 5
typedef int32_t     s32;  // fletcher id : 6

typedef uint64_t    u64;  // fletcher id : 7
typedef int64_t     s64;  // fletcher id : 8

typedef float       f32;  // fletcher id : 9
typedef double      f64;  // fletcher id : 10

const u8 U8_MIN     = 0;
const u8 U8_MAX     = 255;

const s8 S8_MIN     = -128;
const s8 S8_MAX     =  127;

const u16 U16_MIN   = 0;
const u16 U16_MAX   = 65535;

const s16 S16_MIN   = -32768;
const s16 S16_MAX   =  32767;

const u32 U32_MIN   = 0;
const u32 U32_MAX   = +4294967295UL;

const s32 S32_MIN   = -2147483647-1;
const s32 S32_MAX   =  2147483647;


#endif //_STD_SIZE_H_
