/*
 * OSSerial.cpp
 *
 *  Created on: 15 avr. 2012
 *      Author: leduc
 */

#include "OSSerial.hpp"
#include "../RDebug.hpp"

OSSerial::OSSerial()
: _handle (0)
, _timeout(10)
{ }

OSSerial::~OSSerial()
{ }



bool OSSerial::open(const std::string & device, u32 baudrate)
{
  struct termios  options;
  int f_baudrate; //function baudrate

  /* If a device is already opened */
  if (_handle)
  {
    return false;
  }

  /* Baudrate definition */
  switch(baudrate)
  {
    case 2400:
      f_baudrate = B2400;
      break;

    case 4800:
      f_baudrate = B4800;
      break;

    case 9600:
      f_baudrate = B9600;
      break;

    case 19200:
      f_baudrate = B19200;
      break;

    case 38400:
      f_baudrate = B38400;
      break;

    case 57600:
      f_baudrate = B57600;
      break;

    case 115200:
      f_baudrate = B115200;
      break;

    default:
      f_baudrate = 0;
      break;
  }


  if (!f_baudrate)
  {
    rDebug() << "ERROR : Serial " << baudrate << " baudrate unsupported!";
    return false;
  }

  /* get handle device */
  _handle = ::open(device.c_str(), O_RDWR | O_NOCTTY);

  if (_handle < 0)
  {
    _handle = 0;
    return false;
  }

  /* Set up configuration */
  tcgetattr(_handle, &options);

  options.c_cflag = f_baudrate | CS8 | CREAD | CLOCAL;
  options.c_iflag = IGNPAR;
  options.c_oflag = 0;
  options.c_lflag = 0;

  options.c_cc[VMIN]  = 1;
  options.c_cc[VTIME] = 0;

  /* flush the serial and open the device */
  tcsetattr(_handle, TCSANOW, &options);

  return true;
}



bool OSSerial::close()
{
  if (!_handle)
  {
    return false;
  }

  ::close(_handle);
  _handle = 0;

  return true;
}


bool OSSerial::flush()
{
  return tcflush(_handle, TCIFLUSH) == 0;
}


bool OSSerial::timeout(int fd, u32 timeout)
{
  fd_set rfds;
  struct timeval tv;
  int retval;

  FD_ZERO(&rfds);
  FD_SET(fd, &rfds);

  tv.tv_sec = timeout / 1000;
  tv.tv_usec = (timeout * 1000) - (tv.tv_sec * 1000 * 1000);

  retval = select(fd + 1, &rfds, NULL, NULL, &tv);
  if (retval > 0)
  {
    return false;
  }

  if (retval == 0)
  {
    return true;
  }

  return true;
}




bool OSSerial::write(const u8 *buffer, u32 bytes_to_write, u32 *bytes_written)
{
  if (_handle == false)
  {
    return false;
  }

  *bytes_written = 0;
  for (u32 i = 0; i < bytes_to_write; i++)
  {
    s32 r = ::write(_handle, buffer + i, 1);
    if (r < 0)
    {
      return false;
    }

    *bytes_written += r;
  }

  return true;
}


bool OSSerial::read(u8 *buffer, u32 bytes_to_read, u32 *bytes_read)
{
  if (_handle == false)
  {
    return false;
  }

  *bytes_read  = 0;
  for (u32 i = 0; i < bytes_to_read; i++)
  {
    if (timeout(_handle, _timeout))
    {
      return false;
    }

    s32 r = ::read(_handle, buffer + i, 1);
    if (r < 0)
    {
      return false;
    }

    *bytes_read += r;
  }

  return true;
}





