/*
 * OSSerial.hpp
 *
 *  Created on: 15 avr. 2012
 *      Author: leduc
 */

#ifndef OSSERIAL_HPP_
#define OSSERIAL_HPP_

#include "../StdSize.hpp"

// POSIX serial
#include <stdlib.h>
#include <termios.h>
#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#include <string>

class OSSerial
{
public:
  OSSerial();
  ~OSSerial();

  bool open(const std::string & device, u32 baudrate = 115200);
  bool close();
  bool flush();

  bool write(const u8 * buffer, u32 bytes_to_write, u32 *bytes_written);
  bool read (u8 * buffer, u32 bytes_to_read,  u32 *bytes_read);

  void setTimeout(u32 timeout) { _timeout = timeout; };

private:
  int _handle;
  u32 _timeout;

  bool timeout(int fd, u32 timeout);
};

#endif /* OSSERIAL_HPP_ */
