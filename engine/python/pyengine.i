%module(directors="1", allprotected="1") pyEngine

%include <std_string.i>
%include <std_shared_ptr.i>


%{
#include "StdSize.hpp"
#include "Broker.hpp"
#include "AbstractModule.hpp"
#include "RawMessage.hpp"
#include "RThread.hpp"
%}

%shared_ptr(RawMessage)

// Enable cross-language polymorphism in the SWIG wrapper. 
// It's pretty slow so not enable by default
%feature("director") AbstractModule;
%feature("director") AbstractMessage;

// Tell swig to wrap everything
%include "StdSize.hpp"
%include "Broker.hpp"
%include "AbstractModule.hpp"
%include "RawMessage.hpp"
%include "RThread.hpp"

%extend RawMessage 
{
    s8 readS8() 
    { 
        s8 tmp;
        (*$self) >> tmp;
        return tmp;
    }
    
    s16 readS16() 
    { 
        s16 tmp;
        (*$self) >> tmp;
        return tmp;
    }
    
    s16 readS32() 
    { 
        s32 tmp;
        (*$self) >> tmp;
        return tmp;
    }
    
    u8 readU8() 
    { 
        u8 tmp;
        (*$self) >> tmp;
        return tmp;
    }

    u16 readU16() 
    { 
        u16 tmp;
        (*$self) >> tmp;
        return tmp;
    }

    u32 readU32() 
    { 
        u32 tmp;
        (*$self) >> tmp;
        return tmp;
    }

    bool readBool() 
    { 
        bool tmp;
        (*$self) >> tmp;
        return tmp;
    }
    
    f32 readF32()
    {
        f32 tmp;
        (*$self) >> tmp;
        return tmp;       
    }
    
    f64 readF64()
    {
        f64 tmp;
        (*$self) >> tmp;
        return tmp;     
    }
    
    std::string readString()
    {
        std::string tmp;
        (*$self) >> tmp;
        return tmp;
    }
    
    void writeS8(int val)
    {
        s8 res = static_cast<s8>(val);
        (*$self) << res;
    }
    
    void writeU8(int val)
    {
        u8 res = static_cast<u8>(val);
        (*$self) << res;
    }
    
    void writeS16(int val)
    {
        s16 res = static_cast<s16>(val);
        (*$self) << res;
    }
    
    void writeU16(int val)
    {
        u16 res = static_cast<u16>(val);
        (*$self) << res;
    }
    
    void writeS32(int val)
    {
        s32 res = static_cast<s32>(val);
        (*$self) << res;
    }
    
    void writeU32(int val)
    {
        u32 res = static_cast<u32>(val);
        (*$self) << res;
    }
    
    void writeF32(double val)
    {
        f32 res = static_cast<f32>(val);
        (*$self) << res;
    }
    
    void writeF64(double val)
    {
        f64 res = static_cast<f64>(val);
        (*$self) << res;
    }
    
}
