/**
 * \file 	RMap.hpp
 * \author	leduc
 * \date	10 déc. 2011
 */

#ifndef RMAP_HPP_
#define RMAP_HPP_

#include <map>
#include <iostream>

namespace RMap
{


template<typename KEY, typename T>
bool contains(const std::map<KEY, T> & map, const KEY & key)
{
  if(map.count(key) > 0)
    return true;

  return false;
}

template<typename KEY, typename T>
T value(const std::map<KEY, T> & map, const KEY & key)
{
  auto it = map.find(key);
  if(it != map.end())
    return it->second;

  return T(); // return a default construct value
}

template<typename KEY, typename T>
int remove(std::map<KEY, T> & map, const KEY & key)
{
  int oldSize = map.size();
  map.erase(key);           //remove elements
  int newSize = map.size();

  return oldSize - newSize;   //return the number of removed elements
}

template<typename KEY, typename T>
bool insert(std::map<KEY, T> & map, const KEY & key, const T & t)
{
  auto res = map.insert(std::pair<KEY, T>(key, t));
  if(res.second == true)
  {
    return true;
  }

  return false;
}

}

#endif /* RMAP_HPP_ */
