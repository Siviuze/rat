// Inspiré de la classe sf::Packet de la bibliothèque SFML

#ifndef _ABSTRACT_MESSAGE_HPP_
#define _ABSTRACT_MESSAGE_HPP_

#include "StdSize.hpp"

#include <mutex>
#include <vector>
#include <memory>   // smart pointer
#include <string>

#include "EmitterID.hpp"


class AbstractModule;


// helper type
class RawMessage;
typedef std::shared_ptr<RawMessage> psRawMessage;



class RawMessage
{
  friend class Broker;

public:
  static psRawMessage create(const std::string & topic = "");
  virtual ~RawMessage();

  // Header control
  const EmitterID & emitter() const;
  const std::string & topic() const;
  
  void setEmitter(const AbstractModule * pModule);
  void setTopic(const std::string & topic);

  // network
  std::vector<u8> serialize();
  bool unserialize(const std::vector<u8> & msg);

  // access
  void take();
  void release();
  operator bool() const { return _isValid; }

  // fletcher checksum
  u16 getFLetcherChecksum() const;

  // data management
  void clear();

  std::vector<u8> & getDataVector();
  const u8* getData() const;
  std::size_t getDataSize() const;

  // Data extraction
  RawMessage& operator >> (bool&         data);
  RawMessage& operator >> (s8&           data);
  RawMessage& operator >> (u8&           data);
  RawMessage& operator >> (s16&          data);
  RawMessage& operator >> (u16&          data);
  RawMessage& operator >> (s32&          data);
  RawMessage& operator >> (u32&          data);
  RawMessage& operator >> (f32&          data);
  RawMessage& operator >> (f64&          data);
  RawMessage& operator >> (char*         data);
  RawMessage& operator >> (std::string&  data);
  RawMessage& operator >> (wchar_t*      data);
  RawMessage& operator >> (std::wstring& data);

  // Data insertion
  RawMessage& operator << (bool&                data);
  RawMessage& operator << (s8&                  data);
  RawMessage& operator << (u8&                  data);
  RawMessage& operator << (s16&                 data);
  RawMessage& operator << (u16&                 data);
  RawMessage& operator << (s32&                 data);
  RawMessage& operator << (u32&                 data);
  RawMessage& operator << (f32&                 data);
  RawMessage& operator << (f64&                 data);
  RawMessage& operator << (const char*          data);
  RawMessage& operator << (const std::string&   data);
  RawMessage& operator << (const wchar_t*       data);
  RawMessage& operator << (const std::wstring&  data);

protected:
  RawMessage(const std::string& topic);


private:
  void append(const void* data, std::size_t numBytes, std::vector<u8> & toStore);

  bool checkSize(std::size_t size);

  void updateFletcherChecksum(u8 fletcherID);


  std::string       _topic;     // message topic
  EmitterID         _emitter;   // emitter info

  bool              _isValid;   // Reading state of the message
  std::size_t       _readPos;   //
  std::vector<u8>   _data;      // data stored in the message

  std::mutex        _mutex;    // thread-safe access on the message (reading only)

  u16               _fletcherSum1;
  u16               _fletcherSum2;
};



#endif //_ABSTRACT_MESSAGE_HPP_
