#ifndef _ABSTRACT_MODULE_HPP_
#define _ABSTRACT_MODULE_HPP_

#include "StdSize.hpp"

// portable container
#include <string>
#include <list>

#include "RawMessage.hpp"

class Broker;


class AbstractMessage
{
public:
  virtual ~AbstractMessage() { }

  // try to load a RawMessage in members
  virtual bool load(psRawMessage msg)=0;

  // create a RawMessage from members
  virtual psRawMessage pack()=0;

protected:
  AbstractMessage(const std::string & topic, const u16 fletcherChecksum);

  const std::string _topic;
  const u16         _fletcherChecksum;

private:

  // Compute the fletcher's checksum with the size of the in-order data that must be in the message
  u16 fletcherChecksumCompute(const std::vector<u8> & tab);
};



class AbstractModule
{
  friend class Broker;

public:
  AbstractModule(const std::string & type);
  virtual ~AbstractModule();

  s16 slot() const
  { return _slot; }
  
  const std::string & type() const
  { return _type; }
  
  const Broker * broker() const  
  { return _pBroker; }
  
  
protected:
  virtual void setUp()      { }
  virtual void tearDown()   { }

  virtual void newMessage() { };

  void sendMsg(psRawMessage msg);

  psRawMessage getOldestMessage();
  psRawMessage getNewestMessage();


  
private:
  void setSlot(s16 slot) 
  { _slot = slot; }
  
  void setBroker(Broker * b);
  
  std::list<psRawMessage> _messages;
  const std::string       _type;

  s16         _slot;
  Broker *    _pBroker;
};



#endif //_ABSTRACT_MODULE_HPP_
