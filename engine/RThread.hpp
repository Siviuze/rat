/**
 * \file 	RThread.hpp
 * \author	leduc
 * \date	8 avr. 2012
 */

#ifndef RTHREAD_HPP_
#define RTHREAD_HPP_

#include <thread>
#include "StdSize.hpp"


class RThread
{
public:
  RThread();
  virtual ~RThread();

  // Functor
  void operator()() { run(); }

  // Thread facilities
  void launch();
  void join();
  void detach();
  std::thread::id get_id();

  static void msleep(u32 u32Ms)
  { std::this_thread::sleep_for(std::chrono::milliseconds(u32Ms)); }
  static void usleep(u32 u32Ns)
  { std::this_thread::sleep_for(std::chrono::nanoseconds(u32Ns)); }

protected:
  virtual void run() {};

private:
  std::thread * _pThread;
};


#endif /* RTHREAD_HPP_ */
