#include "EmitterID.hpp"

#include "Broker.hpp"
#include "AbstractModule.hpp"

EmitterID::EmitterID (const std::string & moduleType, s16 slot, const std::string & brokerName)
    : _moduleType (moduleType)
    , _slot (slot)
    , _brokerName (brokerName)
{

}


bool EmitterID::isEqual (const EmitterID & type) const
{
  if (_moduleType.empty() != true) // if the string is empty -> there is no filter
  {
    if (_moduleType != type._moduleType)
    {
      return false;
    }

    if (_slot != type._slot)
    {
      return false;
    }
  }

  if (_brokerName.empty() != true) // if the string is empty -> there is no filter
  {
    if (_brokerName != type._brokerName)
    {
      return false;
    }
  }

  return true;
}


bool operator== (const EmitterID& type1, const EmitterID& type2)
{
  return type1.isEqual (type2);
}


void EmitterID::setEmitter(const AbstractModule* pModule)
{
  _brokerName = pModule->broker()->name();
  _moduleType = pModule->type();
  _slot = pModule->slot();
}


const std::string& EmitterID::broker()
{
  return _brokerName;
}

s16 EmitterID::slot()
{
  return _slot;
}

const std::string& EmitterID::type()
{
  return _moduleType;
}

