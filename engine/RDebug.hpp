/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2011  <copyright holder> <email>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#ifndef RDEBUG_H
#define RDEBUG_H

#include "StdSize.hpp"
#include <cstdio>
#include <string>

#define rDebug()    RDebug(RDebug::debug)
#define rWarning()  RDebug(RDebug::warning)
#define rCritical() RDebug(RDebug::critical)
#define rFatal()    RDebug(RDebug::fatal)

#ifndef rOutput
  #define rOutput     stderr
#endif

class RDebug
{ 
public:
  enum
  {
    debug=0,
    warning=1,
    critical=2,
    fatal=3
  };
  
  RDebug(u8 type);
  virtual ~RDebug();
  
  
  RDebug& operator<< (bool val);
  
  RDebug& operator<< (u8 val);
  RDebug& operator<< (s8 val);
  
  RDebug& operator<< (u16 val);
  RDebug& operator<< (s16 val);
  
  RDebug& operator<< (u32 val);
  RDebug& operator<< (s32 val);

  RDebug& operator<< (u64 val);
  RDebug& operator<< (s64 val);

  RDebug& operator<< (f32 val);
  RDebug& operator<< (f64 val);


  RDebug& operator<< (const char * s);

  RDebug& operator<< (const std::string & rString);

  RDebug& operator<< (const void *);

  static void setOutput(FILE * output = stderr) 
  { if(output != NULL) _output = output; }
  
private:
  static FILE * _output;
};

#endif // RDEBUG_H
