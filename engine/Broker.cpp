#include "Broker.hpp"
#include <cstdlib> //rand

Broker::Broker(const std::string name)
    : _name(name)
{

}


Broker::~Broker()
{
  for (auto it = _modules.begin() ; it != _modules.end() ; )
  {
    // remove module from Broker
    remove((it++)->first);  // the item will be removed, thus recovering the following before
  }
}


bool Broker::record(AbstractModule* pModule, s16 requestSlot)
{
  // check if there is free slots
  if (_occupiedSlots.size() >= U16_MAX)
  {
    return false;
  }

  // check if the module was already recorded
  if (RMap::contains(_modules, pModule) == true)
  {
    return false;
  }

  // select an available slot randomly
  if (requestSlot < 0)
  {
    s16 trySlot;
    do
    {
      trySlot = rand() % S16_MAX;
    }
    while (RSet::contains(_occupiedSlots, trySlot) == true);

    addModule(pModule, trySlot);

    return true;
  }

  // check if the requested slot is available
  if (RSet::contains(_occupiedSlots, requestSlot) == true)
  {
    return false;
  }

  addModule(pModule, requestSlot);

  return true;
}


bool Broker::remove(AbstractModule* pModule)
{
  // check if the module is recorded
  if (RMap::contains(_modules, pModule) == false)
  {
    return false;
  }

  // call teardown before removing module
  pModule->tearDown();
  
  // remove slot
  RSet::remove(_occupiedSlots, pModule->slot());
  pModule->setSlot(-1);

  // remove module, destroy filters
  delete RMap::value(_modules, pModule);
  RMap::remove(_modules, pModule);
  pModule->setBroker(NULL);

  return true;
}



bool Broker::addFilter(const AbstractModule* pModule, const std::string& pMsgType, const EmitterID& emId)
{
  return false;
}

bool Broker::rmFilter(const AbstractModule* pModule, const std::string& pMsgType, const EmitterID& emId)
{
  return false;
}


bool Broker::rmFilters(const AbstractModule* pModule)
{
  // check if the module is recorded
  if (RMap::contains(_modules, const_cast<AbstractModule*>(pModule)) == false)
  {
    return false;
  }

  RMap::value(_modules, const_cast<AbstractModule*>(pModule))->clear();

  return true;
}


void Broker::dispatch(psRawMessage msg, AbstractModule* emitter)
{
  auto it = _modules.begin();
  for(; it != _modules.end() ; ++it)
  {
    if(it->first == emitter)
    {
        continue;
    }
  
    // check if the module subscribed to this message
    if(it->second->check(msg))
    {
      it->first->_messages.push_back(msg);  // add the message to the module queue
    }
  }

  for(it = _modules.begin() ; it != _modules.end() ; ++it)
  {
    if(it->first == emitter)
    {
        continue;
    }
  
    it->first->newMessage();          // warn modules of the incoming message
  }

}


void Broker::addModule(AbstractModule* pModule, s16 slot)
{
  // add the slot
  _occupiedSlots.insert(slot);
  pModule->setSlot(slot);

  // add the module
  RMap::insert(_modules, pModule, new Filter());
  pModule->setBroker(this);

  // call setUp
  pModule->setUp();
}

