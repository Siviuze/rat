/*
 * Serial.cpp
 *
 *  Created on: 15 avr. 2012
 *      Author: leduc
 */

#include "Serial.hpp"

#if defined(unix) || defined(__unix__) || defined(__unix) || defined(linux) || \
    defined(__APPLE__) || defined(__NetBSD__) || defined(__OpenBSD__) || defined(__FreeBSD__)
  #include "Unix/OSSerial.hpp"
#else
  #include "Win32/OSSerial.hpp"
#endif



Serial::Serial()
  :_pSerial (new OSSerial)
{
  // TODO Auto-generated constructor stub

}

Serial::~Serial()
{
  delete _pSerial;
}

bool Serial::open(const std::string& device, u32 baudrate)
{
  return _pSerial->open(device, baudrate);
}

bool Serial::close()
{
  return _pSerial->close();
}

bool Serial::flush()
{
  return _pSerial->flush();
}

bool Serial::write(const u8* buffer, u32 bytes_to_write, u32* bytes_written)
{
  return _pSerial->write(buffer, bytes_to_write, bytes_written);
}

bool Serial::read(u8* buffer, u32 bytes_to_read, u32* bytes_read)
{
  return _pSerial->read(buffer, bytes_to_read, bytes_read);
}

void Serial::setTimeout(u32 timeout)
{
  _pSerial->setTimeout(timeout);
}
