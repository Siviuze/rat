/**
 * \file 	RThread.cpp
 * \author	leduc
 * \date	8 avr. 2012
 */

#include "RThread.hpp"



RThread::RThread()
  :_pThread(NULL)
{

}


RThread::~RThread()
{
  if(_pThread != NULL)
  {
    delete _pThread;
  }
}


void RThread::launch()
{
  if(_pThread == NULL)
  {
    _pThread = new std::thread(std::ref(*this));
  }
}


void RThread::join()
{
  if(_pThread != NULL)
  {
    _pThread->join();
  }
}


void RThread::detach()
{
  if(_pThread != NULL)
  {
    _pThread->detach();
  }
}


std::thread::id RThread::get_id()
{
  if(_pThread != NULL)
  {
    return _pThread->get_id();
  }

  return std::thread::id();
}


