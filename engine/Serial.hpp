/*
 * Serial.hpp
 *
 *  Created on: 15 avr. 2012
 *      Author: leduc
 */

#ifndef SERIAL_HPP_
#define SERIAL_HPP_

#include "StdSize.hpp"
#include <string>

class OSSerial;


class Serial
{
public:
  Serial();
  virtual ~Serial();

  bool open(const std::string & port, u32 baudrate = 115200);
  bool close();
  bool flush();

  bool write(const u8 * buffer, u32 bytes_to_write, u32 *bytes_written);
  bool read (u8 * buffer, u32 bytes_to_read,  u32 *bytes_read);

  void setTimeout(u32 timeout);

private:
  OSSerial*  _pSerial;
};

#endif /* SERIAL_HPP_ */
