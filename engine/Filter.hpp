/*
 * Filter.hpp
 *
 *  Created on: 28 févr. 2012
 *      Author: leduc
 */

#ifndef FILTER_HPP_
#define FILTER_HPP_

#include "RawMessage.hpp"
#include "EmitterID.hpp"

#include <string>

#include "RMap.hpp"
#include "RSet.hpp"



class Filter
{
public:
  // check if the message pass module filters
  bool check(const psRawMessage msg) const;
  void clear() { _filters.clear(); }

private:
  std::map<std::string, std::set<EmitterID> >     _filters;
};



#endif /* FILTER_HPP_ */
