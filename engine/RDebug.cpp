/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2011  <copyright holder> <email>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "RDebug.hpp"

FILE * RDebug::_output = stderr;

RDebug::RDebug(u8 type)
{ 
  switch(type)
  {
    case debug:
      fprintf(_output, "Debug: ");
      break;

    case warning:
      fprintf(_output, "Warning: ");
      break;

    case critical:
      fprintf(_output, "Critical: ");
      break;

    case fatal:
      fprintf(_output, "Fatal: ");
      break;

    default:
      break;
  }
}

RDebug::~RDebug()
{
  fprintf(_output, "\n");
  fflush(_output);
}

RDebug& RDebug::operator<<(u64 val)
{
  fprintf(_output, "%lu ", val);

  return *this;
}

RDebug& RDebug::operator<<(s64 val)
{
  fprintf(_output, "%ld ", val);

  return *this;
}

RDebug& RDebug::operator<<(u32 val)
{
  fprintf(_output, "%u ", val);

  return *this;
}

RDebug& RDebug::operator<<(s32 val)
{
  fprintf(_output, "%d ", val);

  return *this;
}

RDebug& RDebug::operator<<(f32 val)
{
  fprintf(_output, "%f ", val);

  return *this;
}
RDebug& RDebug::operator<<(bool val)
{
  if(val == true)
  {
    fprintf(_output, "true ");
  }
  else
  {
    fprintf(_output, "false ");
  }

  return *this;
}

RDebug& RDebug::operator<<(f64 val)
{
  fprintf(_output, "%f ", val);

  return *this;
}

RDebug& RDebug::operator<<(u8 val)
{
  fprintf(_output, "%u ", val);

  return *this;
}

RDebug& RDebug::operator<<(s8 val)
{
  fprintf(_output, "%d ", val);

  return *this;
}

RDebug& RDebug::operator<<(u16 val)
{
  fprintf(_output, "%u ", val);

  return *this;
}

RDebug& RDebug::operator<<(s16 val)
{
  fprintf(_output, "%d ", val);

  return *this;
}

RDebug& RDebug::operator<< (const char * s)
{
  fprintf(_output, "%s ", s);

  return *this;
}

RDebug& RDebug::operator <<(const void * ptr)
{
  fprintf(_output, "%p ", ptr);

  return *this;
}



RDebug& RDebug::operator <<(const std::string & string)
{
  *this << string.c_str();

  return *this;
}

