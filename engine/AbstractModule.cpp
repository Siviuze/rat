#include "AbstractModule.hpp"
#include "Broker.hpp"


AbstractMessage::AbstractMessage(const std::string & topic, const u16 fletcherChecksum)
  :_topic (topic)
  , _fletcherChecksum(fletcherChecksum)
{

}


u16 AbstractMessage::fletcherChecksumCompute(const std::vector<u8> & tab)
{
  u16 sum1 = 0;
  u16 sum2 = 0;

  auto it = tab.begin();
  for(; it != tab.end() ; it++)
  {
    sum1 = (sum1 + *it)  % 255;
    sum2 = (sum2 + sum1) % 255;
  }

  u16 ret = (sum2 << 8) | sum1;
  return ret;
}


AbstractModule::AbstractModule(const std::string & type)
  :_type(type)
{
  _slot = -1;
  _pBroker = NULL;
}


AbstractModule::~AbstractModule()
{
  if(_pBroker != NULL)
  {
    _pBroker->remove(this);
  }
}


void AbstractModule::setBroker(Broker* b)
{
  _pBroker = b;
}


void AbstractModule::sendMsg(psRawMessage msg)
{
    if(_pBroker != NULL)
    {
      _pBroker->dispatch(msg, this);
    }
}


psRawMessage AbstractModule::getOldestMessage()
{
  if(_messages.empty() == true)
  {
    return RawMessage::create("None");
  }

  psRawMessage msg = _messages.front();
  _messages.pop_front();
  return msg;
}



psRawMessage AbstractModule::getNewestMessage()
{
  if(_messages.empty() == true)
  {
    return RawMessage::create("None");
  }

  psRawMessage msg = _messages.back();
  _messages.pop_back();
  return msg;
}
