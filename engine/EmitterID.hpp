#ifndef _EMITTER_ID_HPP
#define _EMITTER_ID_HPP

#include "StdSize.hpp"
#include <string>

class AbstractModule;

class EmitterID
{

public:
  EmitterID (const std::string & moduleType = "",
             s16 slot = 0,
             const std::string & brokerName = "");

  bool isEqual (const EmitterID & type) const;

  const std::string & type();
  s16 slot();
  const std::string & broker();

  void setEmitter(const AbstractModule * pModule);

private:
  std::string   _moduleType;
  s16           _slot;
  std::string   _brokerName;
};

bool operator == (const EmitterID & type1, const EmitterID & type2);


#endif // _EMITTER_ID_HPP
