#include <gtest/gtest.h>
#include "../engine/Broker.hpp"

class TestModule : public AbstractModule
{
  friend class BrokerTest;

public:
  TestModule()
  {
    _start = false;
    _stop = false;
  }

  bool start() { return _start; }
  bool stop()  { return _stop;  }
  u32  rec()   { return _rec;   }

  virtual void newMessage()
  {
    psRawMessage msg = _messages.front();
    _messages.pop();
    msg->take();
    (*msg) >> _rec;
    msg->release();
  }

protected:
  virtual void setUp()
  {
    _start = true;
    _stop = false;
  }

  virtual void tearDown()
  {
    _start = false;
    _stop = true;
  }

private:
  bool _start;
  bool _stop;
  u32 _rec;
};


class BrokerTest : public ::testing::Test
{
public:
  // Remember that SetUp() is run immediately before a test starts.
  virtual void SetUp()
  {
    _m1 = new TestModule();
    ASSERT_EQ(-1, _m1->slot());

    _m2 = new TestModule();
    ASSERT_EQ(-1, _m2->slot());

    _pBroker = new Broker();
  }

  // TearDown() is invoked immediately after a test finishes.
  virtual void TearDown()
  {
    delete _pBroker;
    delete _m1;
    delete _m2;
  }

  Broker *         _pBroker;

  TestModule *     _m1;
  TestModule *     _m2;
};



TEST_F(BrokerTest, RecordTest)
{
  // record a module and check the slot
  ASSERT_TRUE(_pBroker->record(_m1, 17));
  ASSERT_EQ(17, _m1->slot());

  // check setUp()
  ASSERT_TRUE(_m1->start());
  ASSERT_TRUE(!_m1->stop());

  // try to record the module again (with auto slot)
  ASSERT_TRUE(!_pBroker->record(_m1));
  ASSERT_EQ(17, _m1->slot()); // the slot shouldn't change

  // try to record an other module, but with the same slot
  ASSERT_TRUE(!_pBroker->record(_m2, 17));

  // record a second module and check if slot >= 0
  ASSERT_TRUE(_pBroker->record(_m2));
  ASSERT_TRUE(0 < _m2->slot());
}


TEST_F(BrokerTest, RemoveTest)
{
  // try to remove an unrecorded module
  ASSERT_TRUE(!_pBroker->remove(_m1));
  ASSERT_EQ(-1, _m1->slot());

  // init furthers tests
  ASSERT_TRUE(_pBroker->record(_m1));

  // check remove
  ASSERT_TRUE(_pBroker->remove(_m1));
  ASSERT_EQ(-1, _m1->slot());

  // check tearDown()
  ASSERT_TRUE(!_m1->start());
  ASSERT_TRUE (_m1->stop());
}


TEST_F(BrokerTest, DispatchTest)
{
  // record modules for communication
  _pBroker->record(_m1);
  _pBroker->record(_m2);

  // prepare messages
  psRawMessage msg1 = RawMessage::create("test0");
  u32 a = 357;
  (*msg1) << a;
  psRawMessage msg2 = RawMessage::create("test1");
  u32 b = 159;
  (*msg2) << b;

  // send the first one
  _pBroker->dispatch(msg1);

  // check result
  EXPECT_EQ(357, _m1->rec());
  EXPECT_EQ(357, _m2->rec());

  // send the second message
  _pBroker->dispatch(msg2);

  // check result
  ASSERT_EQ(159, _m2->rec());
}

