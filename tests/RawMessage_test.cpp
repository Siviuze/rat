#include <gtest/gtest.h>
#include "../engine/RawMessage.hpp"
#include "../engine/RDebug.hpp"

#include <cstring>
#include <memory>


TEST(RawMessage, Insertion)
{
  psRawMessage raw = RawMessage::create("");

  u8 u8val = 0xAF;
  s8 s8val = 0xFA;

  u16 u16val = 0xAFAF;
  s16 s16val = 0xFAFA;

  u32 u32val = 0xAFAFAFAF;
  s32 s32val = 0xFAFAFAFA;

  f32 f32val = 3.14f;
  f64 f64val = 3.14;

  const char * cStr = "I am  a C string test";
  std::string cppStr = "I am a C++ string test";

  // insert all the stuff
  (*raw) << u8val << s8val << u16val << s16val << u32val << s32val << f32val << f64val << cStr << cppStr;

  // clean
  u8val = u16val = u32val = 0;
  s8val = s16val = s32val = 0;
  f32val = f64val = 0;
  cppStr = "";
  char gStr[40];

  // Extraction
  ASSERT_TRUE(((*raw) >> u8val >> s8val >> u16val >> s16val >> u32val >> s32val >> f32val >> f64val >> gStr >> cppStr));

  // ASSERT_TRUE values
  ASSERT_TRUE(u8val   == 0xAF);
  ASSERT_TRUE(s8val   == (s8)0xFA);
  ASSERT_TRUE(u16val  == 0xAFAF);
  ASSERT_TRUE(s16val  == (s16)0xFAFA);
  ASSERT_TRUE(u32val  == 0xAFAFAFAF);
  ASSERT_TRUE(s32val  == (s32)0xFAFAFAFA);
  ASSERT_TRUE(f32val  == 3.14f);
  ASSERT_TRUE(f64val  == 3.14);
  ASSERT_TRUE(strcmp(gStr, "I am  a C string test") == 0);
  ASSERT_TRUE(cppStr.compare("I am a C++ string test") == 0);
}


TEST(RawMessage, Serialize)
{
  psRawMessage raw = RawMessage::create("test message");

  std::string testData = "I am a C++ string";
  *raw << testData;

  std::vector<u8> serialized = raw->serialize();
  raw->clear(); // to be sure
  ASSERT_TRUE(raw->unserialize(serialized));

  testData.clear();
  *raw >> testData;

  ASSERT_TRUE(testData.compare("I am a C++ string") == 0);
  ASSERT_TRUE(raw->topic().compare( "test message") == 0);
}
