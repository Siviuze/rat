#include <Python.h>
#include <cstdio>


int main(int argc, char *argv[])
{
  FILE * pPythonScript;

  pPythonScript = fopen("main.py", "r");
  Py_Initialize();
  PySys_SetPath(".");

  PyRun_SimpleFile(pPythonScript, "main.py");

  Py_Finalize();
  fclose(pPythonScript);

  return 0;
}
