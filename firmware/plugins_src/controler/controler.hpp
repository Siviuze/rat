 /***********************************************************************
 * Antenatus - Copyright (C) 2011 Leduc Philippe                        *
 *                                                                      *
 * ledphilippe@gmail.com                                                *
 *                                                                      *
 * This file is part of Antenatus.                                      *
 *                                                                      *
 * Antenatus is free software: you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation, either version 3 of the License, or    *
 * (at your option) any later version.                                  *
 *                                                                      *
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *                                                                      *
 * You should have received a copy of the GNU General Public License    *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.*
 *                                                                      *
 ***********************************************************************/


#ifndef _CONTROLER_HPP_
#define _CONTROLER_HPP_

#include <api.hpp>
#include <QDebug>

class Controler : public PluginInterface
{
  Q_OBJECT
public:
  Controler();
  virtual ~Controler() {}

  void getMessage(structMess * pm);

public slots:
  void recorded_slot();
};


class ControlerFactory : public QObject, public PluginFactory
{
  Q_OBJECT
  Q_INTERFACES(PluginFactory)

public:
  virtual PluginInterface * createPlugin();
};


#endif
