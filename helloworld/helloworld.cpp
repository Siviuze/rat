#include <AbstractModule.hpp>
#include <RDebug.hpp>

class HelloMessage : public AbstractMessage
{
public:
    HelloMessage() : AbstractMessage("HelloMessage", 0x2f14) // 11 : string, 6 : string length, 4 repeat
    { }
    
    virtual psRawMessage pack()
    {
        psRawMessage toSend = RawMessage::create(_topic);   // create the underlying message
        (*toSend) << msg << repeat;                         // stack the data (order is important)
        return toSend;
    }
    
    virtual bool load(psRawMessage toLoad)
    {        
        // check the topic
        if(toLoad->topic() != _topic)
        {
            rDebug() << "Bad topic";
            return false;
        }
        
        // check the fletcher checksum (order of data type in the message)
        if(toLoad->getFLetcherChecksum() != this->_fletcherChecksum)
        {
            rDebug() << "Bad checksum";
            rDebug() << "Get      " << toLoad->getFLetcherChecksum();
            rDebug() << "Should be" << this->_fletcherChecksum;
            return false;
        }
        
        
        toLoad->take();                 //lock the message acces and init the reader
        (*toLoad) >> msg >> repeat;     // extract data
        toLoad->release();              //unlock the message
        
        return true;
    }
    
    std::string     msg;
    s16             repeat;
};


class HelloModule : public AbstractModule
{
public:
    HelloModule() : AbstractModule("HelloModule")
    { }
    
    virtual void newMessage() // this method is called each time a new message is distributed to this module
    { 
        psRawMessage incoming = getOldestMessage();
        
        HelloMessage helloMsg;
        if(helloMsg.load(incoming) == true)
        {
            for(s16 i=0 ; i<helloMsg.repeat ; i++)
            {
                rDebug() << helloMsg.msg;
            }
        }
    }
};


#include <Broker.hpp>

int main()
{
    Broker br;
    
    HelloModule helloWorld;
    HelloMessage toSend;
    
    br.record(&helloWorld);
    
    toSend.msg    = "Hello World ! ! !";
    toSend.repeat = 4;
    br.dispatch(toSend.pack(), NULL);
    
    br.remove(&helloWorld);
    
    return 0;
}